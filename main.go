package main

import (
	"fmt"

	util "gobernetes.ro/utils"
	containerManager "gobernetes.ro/utils/docker"
)

func main() {

	fmt.Println(util.Test())
	containerManager.StartContainer("docker.io/library/alpine", []string{"echo", "hello world"}, "myAlpine")

}
